﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System;
using System.Threading;

namespace MWBTest

{
    class Program
    {
        IWebDriver driver = new ChromeDriver("C:\\Program Files (x86)\\Google\\Chrome\\Application");


        [SetUp]
        public void start()
        {
            Console.WriteLine("test started");
        }
        
        [Test]
        public void AllTest()
        {
            Login(true);
            TestHomePage();
            Thread.Sleep(1000);
            review();
            Thread.Sleep(1000);
            TestOnHours();
            Thread.Sleep(1000);
            TestOnHoursNotFilled();
            Thread.Sleep(1000);
            TestOnEmployees();
            Thread.Sleep(1000);
            CaoTest();
            Thread.Sleep(1000);
            TestOnProject();
            Thread.Sleep(1000);
            ValidityDcoument();
            Thread.Sleep(1000);
            AdminLeavePage();
            Thread.Sleep(1000);
            testEmployeeData();
            Thread.Sleep(1000);
            Logout();
            Thread.Sleep(1000);
            Login(false);
            Thread.Sleep(1000);
            UserDashboard();
            Thread.Sleep(1000);
            UserHours();
            Thread.Sleep(1000);
            UserLeaves();
            Thread.Sleep(1000);
            UserLeaveOverview();
            Thread.Sleep(1000);
            
            Logout();
        }

        [Test]
       public void Logout()
        {
            //Login(true);
            IWebElement user = driver.FindElement(By.XPath("/html/body/div[1]/div[2]/div[3]/nav/div/a"));
            user.Click();
            Thread.Sleep(1000);
            IWebElement logout = driver.FindElement(By.XPath("/html/body/div[1]/div[2]/div[3]/nav/div/div/ul/li[2]/a"));
            logout.Click();
            Thread.Sleep(1000);
        }

        public void FreeDays()
        {
            Login(true);
            driver.Navigate().GoToUrl("https://mwbtest.eigensteil.nl/dashboard/");
            Thread.Sleep(2000);

        }

        [Test]
        public void TestHomePage()

        {
            //Login(true);
            driver.Navigate().GoToUrl("https://mwbtest.eigensteil.nl/dashboard/");
            Thread.Sleep(2000);
            IWebElement NavigateToHours = driver.FindElement(By.XPath("/ html / body / div [1] / div [3] / div / div / div [2] / div / div [2] / div / div / table / tbody / tr [1] / td [5] / i"));
            NavigateToHours.Click();
            Console.WriteLine("Navigate Hours Link Button Working \n");

            driver.Navigate().GoToUrl("https://mwbtest.eigensteil.nl/dashboard/");
            Thread.Sleep(1000);
            IWebElement NavigateToLeaves = driver.FindElement(By.XPath("/ html / body / div [1] / div [3] / div / div / div [3] / div / div [2] / div / div / table / tbody / tr / td [7] / i"));
            NavigateToLeaves.Click();
            Console.WriteLine("Navigate Leaves Link Button Working \n");
            Thread.Sleep(1000);
            driver.Navigate().GoToUrl("https://mwbtest.eigensteil.nl/dashboard/");
            Thread.Sleep(1000);
            search("(//*[@id='search'])[position()=1]");
            search("(//*[@id='search'])[position()=2]");

        }



        [Test]
        public void review()
        {
          //  //Login(true);
            Thread.Sleep(2000);
            driver.Navigate().GoToUrl("https://mwbtest.eigensteil.nl/Klanttevredenheid/-1");
            //test is created for tesing cusomer review entry
            IWebElement Topic = driver.FindElement(By.Name("Onderwerp"));
            Topic.SendKeys("Service");
            IWebElement CName = driver.FindElement(By.Name("Naam klant"));
            CName.SendKeys("shubham digole");
            IWebElement Address = driver.FindElement(By.Name("Adres"));
            Address.SendKeys("latur");
            IWebElement Postcode = driver.FindElement(By.Name("Postcode"));
            Postcode.SendKeys("413512");
            IWebElement City = driver.FindElement(By.Name("Plaats"));
            City.SendKeys("latur");
            IWebElement Email = driver.FindElement(By.Name("E-mailadres"));
            Email.SendKeys("shubhamdigole@gmail.com");

            SubmitButton();
            Console.Write("Data Saved \n");
            Thread.Sleep(2000);
            var toast = driver.FindElement(By.ClassName("toast-message")).Text;
            Assert.AreEqual(toast, "De gegevens zijn succesvol opgeslagen", "Updated Data Not saved Properly ");

            IWebElement sendmail = driver.FindElement(By.ClassName("cursorpointer"));
            sendmail.Click();
            Thread.Sleep(2000);

            Console.WriteLine("mail sent \n");

            Thread.Sleep(2000);
            editButton("/ html / body / div[1] / div[3] / div / div / div[2] / div / div / table / tbody / tr[1] / td[6] / i");
            pagination();
            search();

        }

        [Test]
        public void TestOnHours()
        {
            //Login(true);
            Thread.Sleep(2000);
            driver.Navigate().GoToUrl("https://mwbtest.eigensteil.nl/hours/ingediendeuren/");
            search();
            Select2DropDown("Goedgekeurd");
            IWebElement clickbutton = driver.FindElement(By.XPath("/html/body/div[1]/div[3]/div/div/div[2]/div/div/div/div[2]/div/div/table/tbody/tr[2]/td[5]/i"));
            clickbutton.Click();

        }
        [Test]
        public void TestOnHoursNotFilled()
        {
            //Login(true);
            Thread.Sleep(2000);
            driver.Navigate().GoToUrl("https://mwbtest.eigensteil.nl/hours/nietingedienduren/");
            search();

        }

       

        [Test]
        public void TestOnProject()
        {
            //Login(true);
            driver.Navigate().GoToUrl("https://mwbtest.eigensteil.nl/projecten/");
            IWebElement radio = driver.FindElement(By.XPath("//input[@value='false']"));
            Console.WriteLine("radio are Working");
            radio.Click();
            Thread.Sleep(2000);
            AddButton("/ html / body / div [1] / div [3] / div / div / div [1] / div / div [2] / div [1] / a");
            IWebElement Topic = driver.FindElement(By.Name("Code"));
            Topic.SendKeys("1234");
            IWebElement Discription = driver.FindElement(By.Name("Omschrijving"));
            Discription.SendKeys("Testing Test");
            IWebElement EndDate = driver.FindElement(By.XPath("/html/body/div[1]/div[3]/div/div/form/div[2]/div/div/div/div[4]/div[2]/div/input"));
            EndDate.Click();
            IWebElement ChangeMonth = driver.FindElement(By.XPath("/ html / body / div[1] / div[3] / div / div / form / div[2] / div / div / div / div[4] / div[2] / div / div / div / div[1] / a[7]"));
            ChangeMonth.Click();

            IWebElement date = driver.FindElement(By.XPath("/html/body/div[1]/div[3]/div/div/form/div[2]/div/div/div/div[4]/div[2]/div/div/div/div[2]/div[1]/a[10]"));
            date.Click();
            SubmitButton();
            editButton("/ html / body / div [1] / div [3] / div / div / div [2] / div / div / table / tbody / tr [2] / td [5] / div / i [1]");
            search();

        }

        [Test]
        public void UserHours()
        {
            Login(false);
            driver.Navigate().GoToUrl("https://mwbtest.eigensteil.nl/uren/");
            Thread.Sleep(5000);
            int number = new Random().Next(99) + 30;
            IWebElement nextweek = driver.FindElement(By.XPath("/html/body/div[1]/div[3]/div/div/div[2]/div/div[1]/div[2]/div/div[1]/input"));
            nextweek.SendKeys(""+ number +"");
            Thread.Sleep(5000);
            IWebElement add = driver.FindElement(By.XPath("/html/body/div[1]/div[3]/div/div/div[2]/div/div[2]/div/div/table/tbody/tr[1]/td[1]/i"));
            add.Click();
            Thread.Sleep(1000);
            IWebElement project = driver.FindElement(By.XPath("/html/body/div[1]/div[3]/div/div/div[2]/div/div[2]/div/div/table/tbody/tr[1]/td[5]/div/span/span[1]/span"));
            project.Click();
            Thread.Sleep(1000);
            IWebElement selectproject = driver.FindElement(By.XPath("/html/body/span/span/span[1]/input"));
            selectproject.SendKeys("003" + Keys.Enter);
            Thread.Sleep(1000);
            IWebElement traveltime = driver.FindElement(By.XPath("/html/body/div[1]/div[3]/div/div/div[2]/div/div[2]/div/div/table/tbody/tr[1]/td[6]/input"));
            traveltime.SendKeys("2");
            Thread.Sleep(1000);
            IWebElement add2 = driver.FindElement(By.XPath("/html/body/div[1]/div[3]/div/div/div[2]/div/div[2]/div/div/table/tbody/tr[1]/td[1]/i"));
            add2.Click();
            Thread.Sleep(1000);
          /*  IWebElement delete = driver.FindElement(By.XPath("/html/body/div[1]/div[3]/div/div/div[2]/div/div[2]/div/div/table/tbody/tr[2]/td[14]/i"));
            delete.Click();*/
            Thread.Sleep(1000);
            IWebElement save = driver.FindElement(By.XPath("/html/body/div[1]/div[3]/div/div/div[2]/div/div[1]/div[3]/div[2]/button"));
            save.Click();
            Thread.Sleep(1000);
            IWebElement submit = driver.FindElement(By.XPath("/html/body/div[1]/div[3]/div/div/div[2]/div/div[1]/div[3]/div[3]/button"));
            submit.Click();
            Thread.Sleep(2000);
             IWebElement confirm = driver.FindElement(By.ClassName("dg-btn--ok"));
            confirm.Click();
            Thread.Sleep(3000);


        }


        [Test]
        public void TestModal()
        {
            //Login(true);
            driver.Navigate().GoToUrl("https://mwbtest.eigensteil.nl/werknemer/-1");
            Thread.Sleep(3000);
            
            IWebElement initial = driver.FindElement(By.XPath("/html/body/div[1]/div[1]/div[2]/div/a[4]/div"));
            initial.Click();
            Thread.Sleep(1000);
            IWebElement Name = driver.FindElement(By.ClassName("dg-btn--ok"));
            Name.Click();
            Thread.Sleep(10000);
        }

        [Test]
        public void TestOnEmployees()
        {
            //Login(true);
            driver.Navigate().GoToUrl("https://mwbtest.eigensteil.nl/werknemer/");
            Thread.Sleep(3000);
            AddButton("/ html / body / div [1] / div [3] / div / div / div [1] / div / div [2] / div [1] / a");
            Thread.Sleep(2000);
            IWebElement initial = driver.FindElement(By.XPath("/ html / body / div [1] / div [3] / div / div / form / div [1] / div / div [1] / div [2] / input"));
            initial.SendKeys("Testing Test");
            IWebElement Name = driver.FindElement(By.XPath("/ html / body / div [1] / div [3] / div / div / form / div [1] / div / div [2] / div [2] / input"));
            Name.SendKeys("Shubham");
            IWebElement Insertion = driver.FindElement(By.XPath("/ html / body / div [1] / div [3] / div / div / form / div [1] / div / div [3] / div [2] / input"));
            Insertion.SendKeys("Abcd");
            IWebElement Surname = driver.FindElement(By.XPath("/ html / body / div [1] / div [3] / div / div / form / div [1] / div / div [4] / div [2] / input"));
            Surname.SendKeys("Digole");
            Thread.Sleep(2000);
            IWebElement Date = driver.FindElement(By.XPath("/ html / body / div [1] / div [3] / div / div / form / div [1] / div / div [5] / div [2] / div / input"));
            Date.Click();
            Thread.Sleep(2000);

            Thread.Sleep(2000);
            IWebElement date = driver.FindElement(By.XPath("/html/body/div[1]/div[3]/div/div/form/div[1]/div/div[5]/div[2]/div/div/div/div[2]/div[1]/a[14]"));
            date.Click();
            Thread.Sleep(2000);
            IWebElement Street = driver.FindElement(By.XPath("/ html / body / div [1] / div [3] / div / div / form / div [1] / div / div [6] / div [2] / input"));
            Street.SendKeys("mg raod");
            IWebElement Houseno = driver.FindElement(By.XPath("/ html / body / div [1] / div [3] / div / div / form / div [1] / div / div [4] / div [2] / input"));
            Houseno.SendKeys("101");
            IWebElement PostalCode = driver.FindElement(By.XPath("/ html / body / div [1] / div [3] / div / div / form / div [1] / div / div [4] / div [2] / input"));
            PostalCode.SendKeys("413512");
            IWebElement StartDate = driver.FindElement(By.XPath("/ html / body / div [1] / div [3] / div / div / form / div [1] / div / div [10] / div [2] / div / input"));
            StartDate.Click();
            Thread.Sleep(2000);


            IWebElement date2 = driver.FindElement(By.XPath("/html/body/div[1]/div[3]/div/div/form/div[1]/div/div[10]/div[2]/div/div/div/div[2]/div[1]/a[19]"));
            date2.Click();
            Thread.Sleep(2000);
            IWebElement Phoneno = driver.FindElement(By.XPath("/ html / body / div [1] / div [3] / div / div / form / div [1] / div / div [12] / div [2] / input"));
            Phoneno.SendKeys("777858952");
            int number = new Random().Next(111) + 30;


            IWebElement email = driver.FindElement(By.XPath("/html/body/div[1]/div[3]/div/div/form/div[1]/div/div[13]/div[2]/input"));
            email.SendKeys("shubhamdigole" + number + "@gmail.com");
            Thread.Sleep(2000);
            IWebElement access = driver.FindElement(By.XPath("/html/body/div[1]/div[3]/div/div/form/div[1]/div/div[14]/div[2]/div/div[2]/label/span"));
            access.Click();
            Thread.Sleep(1000);
            IWebElement Password = driver.FindElement(By.XPath("/html/body/div[1]/div[3]/div/div/form/div[1]/div/div[15]/div[2]/input"));
            Password.SendKeys("P@ssw0rd");
            IWebElement ConfirmPassword = driver.FindElement(By.XPath("/html/body/div[1]/div[3]/div/div/form/div[1]/div/div[16]/div[2]/input"));
            ConfirmPassword.SendKeys("P@ssw0rd");
            SubmitButton();
            Thread.Sleep(1000);
            
            IWebElement confirm = driver.FindElement(By.ClassName("dg-btn--ok"));
            confirm.Click();
            Thread.Sleep(3000);

            editButton("/html/body/div[1]/div[3]/div/div/div[2]/div/div/table/tbody/tr[1]/td[4]/i[1]");
            Thread.Sleep(1000);
            IWebElement detailspage = driver.FindElement(By.XPath("/html/body/div[1]/div[3]/div/div/div[2]/div/div/table/tbody/tr[1]/td[3]/a"));
            detailspage.Click();
            Thread.Sleep(3000);
            driver.Navigate().GoToUrl("https://mwbtest.eigensteil.nl/werknemer/");
            Thread.Sleep(1000);
            search("//*[@id='search']");
        }
        [Test]
        public void testEmployeeData()
        {
            //Login(true);
            driver.Navigate().GoToUrl("https://mwbtest.eigensteil.nl/instellingen/medewerker-data/");

            AddButton("/ html / body / div [1] / div [3] / div / div / div [1] / div / div [2] / div [1] / a");
            IWebElement CName = driver.FindElement(By.Name("Omschrijving"));
            CName.SendKeys("Testing Test");
            Select2DropDown("Registratie");
            IWebElement Postcode = driver.FindElement(By.Name("Herinneringen in dagen"));
            Postcode.SendKeys("132");
            SubmitButton();
            editButton("/ html / body / div[1] / div[3] / div / div / div[2] / div / div / table / tbody / tr / td[4] / i[1]");
            search();
        }

        [Test]
        public void CaoTest()
        {
            Login(true);
            driver.Navigate().GoToUrl("https://mwbtest.eigensteil.nl/instellingen/cao/");
            Thread.Sleep(1000);
            driver.FindElement(By.XPath("/html/body/div[1]/div[3]/div/div/div[1]/div/div[2]/div[1]/a")).Click();
            Thread.Sleep(1000);
            driver.FindElement(By.XPath("/html/body/div[1]/div[3]/div/div/form/div/div/div/div/div[1]/div[2]/input")).SendKeys("Tets CEO");
            Thread.Sleep(1000);
            driver.FindElement(By.XPath("//*[@id='select2--container']")).Click();
            Thread.Sleep(1000);
            driver.FindElement(By.XPath("/html/body/span/span/span[1]/input")).SendKeys("Automatisch" + Keys.Enter);
            Thread.Sleep(1000);
            driver.FindElement(By.XPath("//*[@id='Submit']")).Click();

            Thread.Sleep(3000);
           
            editButton("/html/body/div[1]/div[3]/div/div/div[2]/div/div/table/tbody/tr[1]/td[3]/i[1]");
            Thread.Sleep(1000);
            search("//*[@id='search']");
        }

        [Test]
        public void ValidityDcoument()
        {
            //Login(true);
            driver.Navigate().GoToUrl("https://mwbtest.eigensteil.nl/werknemer/1/profiel");
            Thread.Sleep(1000);
            driver.FindElement(By.XPath("/html/body/div[1]/div[3]/div/div/form/div/div[2]/div/div/div/div[2]/div/button")).Click();
            Thread.Sleep(1000);
            Select2DropDown("tssta");
            IWebElement StartDate = driver.FindElement(By.XPath("/html/body/div[1]/div[4]/div/div/div/div[1]/div/div[2]/div[2]/div/input"));
            StartDate.Click();
            Thread.Sleep(2000);


            IWebElement date2 = driver.FindElement(By.XPath("/html/body/div[1]/div[4]/div/div/div/div[1]/div/div[2]/div[2]/div/div/div/div[2]/div[1]/a[18]"));
            date2.Click();
            Thread.Sleep(2000);
            IWebElement save = driver.FindElement(By.XPath("/html/body/div[1]/div[4]/div/div/div/div[2]/div/button[1]"));
            save.Click();
            Thread.Sleep(2000);
            UploadDcoument();
        }
      

        [Test]
        public void UserLeaves()
        {
            Login(false);
            driver.Navigate().GoToUrl("https://mwbtest.eigensteil.nl/verlofaanvraag/");
            Thread.Sleep(2000);
            IWebElement daypart = driver.FindElement(By.XPath("/html/body/div[1]/div[3]/div/div/div[2]/div/div/div/div/form/div[1]/div[2]/div[2]/label/input"));
            daypart.Click();
            IWebElement date = driver.FindElement(By.XPath("/html/body/div[1]/div[3]/div/div/div[2]/div/div/div/div/form/div[2]/div[2]/div/input"));
            date.Click();
            IWebElement day = driver.FindElement(By.XPath("/html/body/div[1]/div[3]/div/div/div[2]/div/div/div/div/form/div[2]/div[2]/div/div/div/div[2]/div[1]/a[17]"));
            day.Click();
            Thread.Sleep(1000);
    
            IWebElement details = driver.FindElement(By.XPath("/html/body/div[1]/div[3]/div/div/div[2]/div/div/div/div/form/div[5]/div[2]/textarea"));
            details.SendKeys("teting sw");
            Thread.Sleep(1000);
           
            IWebElement submit = driver.FindElement(By.XPath("//*[@id='Submit']"));
            submit.Click();
            Thread.Sleep(2000);
            IWebElement confirm = driver.FindElement(By.XPath("/html/body/div[1]/div[4]/div/div/div/div[2]/div/button[1]"));
            confirm.Click();
            Thread.Sleep(2000);

        }
        [Test]
        public void UserDashboard()
        {
            Login(false);
            Thread.Sleep(1000);
            IWebElement weekstatus = driver.FindElement(By.XPath("/html/body/div[1]/div[3]/div/div/div[2]/div/div/div/div[2]/div/div/table/tbody/tr[1]/td[3]/i"));
            weekstatus.Click();
            Console.WriteLine("link is working");
            Thread.Sleep(2000);
            
        }
        [Test]
        public void UserLeaveOverview()
        {
            Login(false);
            driver.Navigate().GoToUrl("https://mwbtest.eigensteil.nl/verlofoverzicht/");
            Thread.Sleep(1000);
            search("//*[@id='search']");
        }
        [Test]
        public void AdminLeavePage()
        {
            //Login(true);
            driver.Navigate().GoToUrl("https://mwbtest.eigensteil.nl/verlofs/");
            Thread.Sleep(2000);
            IWebElement details = driver.FindElement(By.XPath("/html/body/div[1]/div[3]/div/div/div[2]/div/div[2]/div/div[1]/table/tbody/tr[2]/td[6]/i"));
            details.Click();
            Thread.Sleep(2000);
            IWebElement input = driver.FindElement(By.XPath("/ html / body / div [1] / div [3] / div / div / form / div / div / div [2] / div [2] / div / div / div [2] / div / textarea"));
            input.SendKeys("test");
            Thread.Sleep(2000);
            IWebElement submit = driver.FindElement(By.XPath("/ html / body / div [1] / div [3] / div / div / form / div / div / div [2] / div [2] / div / div / div [3] / div / button"));
            submit.Click();
            Console.WriteLine("Leave Response successfully");
            driver.Navigate().GoToUrl("https://mwbtest.eigensteil.nl/verlofs/");
            Thread.Sleep(2000);
            IWebElement StartDate = driver.FindElement(By.XPath("/ html / body / div [1] / div [3] / div / div / div [2] / div / div [1] / div / div [3] / div / input"));
            StartDate.Click();
            Console.WriteLine("date filter applied");
            Thread.Sleep(2000);

            IWebElement date = driver.FindElement(By.XPath("/html/body/div[1]/div[3]/div/div/div[2]/div/div[1]/div/div[3]/div/div/div/div[2]/div[1]/a[10]"));
            date.Click();
            Thread.Sleep(5000);
            Select2DropDown("Afgekeurd");
            //search();
        }

        [TearDown]
        public void Stop()
        {
            driver.Quit();
            Thread.Sleep(3000);
            Console.Write("test ended \n");
        }



        public void editButton(string xpath)
        {
            IWebElement editbtn = driver.FindElement(By.XPath(xpath));
            editbtn.Click();
            Thread.Sleep(4000);
            SubmitButton();
            var toast2 = this.toast();
            Assert.AreEqual(toast2, "De gegevens zijn succesvol opgeslagen", "Updated Data Not saved Properly ");
            Thread.Sleep(3000);
        }

        public void Select2DropDown(string value)
        {
            IWebElement Arrow = driver.FindElement(By.ClassName("select2-selection__arrow"));
            Arrow.Click();
            Thread.Sleep(3000);
            driver.FindElement(By.ClassName("select2-search__field")).SendKeys(value + Keys.Enter);
            Thread.Sleep(3000);

        }

        public void DeleteButton(string xpath)
        {
            IWebElement DeleteButton = driver.FindElement(By.XPath(xpath));
            DeleteButton.Click();
            Thread.Sleep(1000);
            var toast2 = this.toast();
            Assert.IsTrue(toast2 == "De gegevens zijn succesvol verwijderd" || toast2 == "Dit record kan niet worden verwijderd", "Delete Button working Properly");
            Thread.Sleep(3000);
        }
        public void pagination()
        {
            IWebElement pagebutton = driver.FindElement(By.XPath("/ html / body / div [1] / div [3] / div / div / div [2] / div / div [2] / nav / ul / li [4] / button"));
            pagebutton.Click();
            Console.WriteLine("pagination \n");
            Thread.Sleep(3000);
        }

        public string toast()
        {
            var toast = driver.FindElement(By.ClassName("toast-message")).Text;
            return toast;
        }

        public void SubmitButton()
        {
            IWebElement savereview = driver.FindElement(By.Name("Submit"));
            savereview.Click();
            Thread.Sleep(2000);
        }

        public void AddButton(string xpath)
        {
            IWebElement addbtn = driver.FindElement(By.XPath(xpath));
            addbtn.Click();
            Thread.Sleep(4000);
        }
        public void search(string? xpath = null)
        {
            IWebElement searchinput;
            Thread.Sleep(2000);
            if (xpath == null)
            {
                searchinput = driver.FindElement(By.Id("search"));
            }
            else
            {
                searchinput = driver.FindElement(By.XPath(xpath));
            }
            searchinput.SendKeys("a");
            Thread.Sleep(3000);
            Console.WriteLine("search is working \n");
        }


       
        public void Login(Boolean isAdmin)
        {

            driver.Navigate().GoToUrl("https://mwbtest.eigensteil.nl/login");
            driver.Manage().Window.Maximize();

            IWebElement ele = driver.FindElement(By.Name("username"));
            if (isAdmin) ele.SendKeys("admin@eigensteil.nl");
            else ele.SendKeys("arthur@eigensteil.nl");
            Thread.Sleep(2000);
            IWebElement ele2 = driver.FindElement(By.Name("password"));
            ele2.SendKeys("P@ssw0rd");

            Thread.Sleep(2000);
            SubmitButton();
            Thread.Sleep(3000);
            Console.Write("login button is clickd \n");
        }

        public void UploadDcoument()
        {
            driver.FindElement(By.XPath("//html/body/div[1]/div[3]/div/div/form/div/div[3]/div/div/div/div[2]/div/button")).Click();

            Thread.Sleep(1000);

            IWebElement StartDate = driver.FindElement(By.XPath("/html/body/div[1]/div[4]/div/div/div/div[1]/div/div/div[2]/input"));
            StartDate.SendKeys("C:\\Users\\Shree\\Documents\\Bandicam\\employyedata.mp4");
            Thread.Sleep(2000);
            IWebElement save = driver.FindElement(By.XPath("/html/body/div[1]/div[4]/div/div/div/div[2]/div/button[1]"));
            save.Click();
            Thread.Sleep(2000);
        }

    }
}
